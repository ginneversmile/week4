package factory;

public class Factory {
    public Plan getPlan(String planType) {
        if(planType == null) {
            return null;
        }
        if(planType.equalsIgnoreCase("DOMESTICPLAN")) {
            return new DomesticPlan();
        }
        return new CommercialPlan();
    }

    public static void main(String[] args) {
        Factory f = new Factory();
        String planName = "commercialplan";
        Plan p = f.getPlan(planName);
        p.getRate();
        p.calculateBill(10);
        Plan p1 = f.getPlan("");
        p.getRate();
        p.calculateBill(5);
    }
}

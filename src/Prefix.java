public class Prefix {
    int[] arr;

    public Prefix(int[] arr) {
        this.arr = arr;
    }
    private void fillPrefixSum(int[] arr, int n , int[] prefixSum) {
        prefixSum[0] = arr[0];
        for (int i = 1; i < n; ++i)
            prefixSum[i] = prefixSum[i - 1] + arr[i];
    }

    public int[] compute() {
       int[] arr = new int[this.arr.length];
        fillPrefixSum(this.arr, this.arr.length,arr);
       return arr;
    }

    public static void main(String[] args) {
        int[] a = { 10, 4, 16, 20};
        Prefix p = new Prefix(a);
        int []b = p.compute();
        for (int i:b) {
            System.out.println(i);
        }
    }
    
}

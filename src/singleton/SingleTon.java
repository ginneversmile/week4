package singleton;

public class SingleTon {
    static SingleTon instance;
    private SingleTon() {

    }
    public static SingleTon getInstance() {
        if(instance == null) {
            instance = new SingleTon();
        }
        return instance;
    }
    public void whoami() {
        System.out.println(this.hashCode());
    }
    public static void main(String[] args) {
        SingleTon instance1 = SingleTon.getInstance();
        SingleTon instance2 = SingleTon.getInstance();
        instance1.whoami();
        instance2.whoami();
        System.out.println(instance1 == instance2);
    }
}
